defmodule Waker.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Waker.Repo,
      # Start the Telemetry supervisor
      WakerWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Waker.PubSub},
      {Finch, name: Waker.Finch},
      # Start the Endpoint (http/https)
      WakerWeb.Endpoint
      # Start a worker by calling: Waker.Worker.start_link(arg)
      # {Waker.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Waker.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    WakerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
