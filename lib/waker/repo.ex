defmodule Waker.Repo do
  use Ecto.Repo,
    otp_app: :waker,
    adapter: Ecto.Adapters.Postgres
end
