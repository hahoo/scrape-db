defmodule Waker.Bid do
  @moduledoc """
  The Bid context.
  """

  import Ecto.Query, warn: false
  alias Waker.Repo

  alias Waker.Bid.{Offer, OfferLink}

  @doc """
  Returns the list of offers.

  ## Examples

      iex> list_offers()
      [%Offer{}, ...]

  """
  def list_offers do
    Repo.all(Offer)
  end

  @doc """
  Gets a single offer.

  Raises `Ecto.NoResultsError` if the Offer does not exist.

  ## Examples

      iex> get_offer!(123)
      %Offer{}

      iex> get_offer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_offer!(id), do: Repo.get!(Offer, id)

  @doc """
  Creates a offer.

  ## Examples

      iex> create_offer(%{field: value})
      {:ok, %Offer{}}

      iex> create_offer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_offer(attrs \\ %{}) do
    %Offer{}
    |> Offer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a offer.

  ## Examples

      iex> update_offer(offer, %{field: new_value})
      {:ok, %Offer{}}

      iex> update_offer(offer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_offer(%Offer{} = offer, attrs) do
    offer
    |> Offer.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a offer.

  ## Examples

      iex> delete_offer(offer)
      {:ok, %Offer{}}

      iex> delete_offer(offer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_offer(%Offer{} = offer) do
    Repo.delete(offer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking offer changes.

  ## Examples

      iex> change_offer(offer)
      %Ecto.Changeset{data: %Offer{}}

  """
  def change_offer(%Offer{} = offer, attrs \\ %{}) do
    Offer.changeset(offer, attrs)
  end

  def change_link(%OfferLink{} = offer_link, attrs \\ %{}) do
    OfferLink.changeset(offer_link, attrs)
  end

  def check_offer(link) do
    link = rework_link(link)
    changeset = %OfferLink{} |> OfferLink.changeset(%{link: link})

    with true <- changeset.valid?,
         {:ok, %{price: price, name: name}} <- make_request(link) do
      offer_changeset =
        %Offer{}
        |> Offer.changeset(%{title: name, link: link, price: price, type: :allegro})

      {:ok, offer_changeset}
    else
      false -> OfferLink.validate(changeset)
      :error -> {:error, changeset}
    end
  end

  defp make_request(link) do
    req = Finch.build(:get, link)

    with {:ok, %Finch.Response{body: body}} <- Finch.request(req, Waker.Finch),
         {:ok, document} = Floki.parse_document(body),
         name = parse_name(document),
         price = parse_price(document) do
      {:ok, %{price: price, name: name}}
    else
      _ -> :error
    end
  end

  defp rework_link(link) do
    link
    |> String.split("?")
    |> List.first()
  end

  defp parse_name(document) do
    document
    |> Floki.find("div[data-box-name=\"showoffer.productHeader\"] h4")
    |> Floki.text()
  end

  defp parse_price(document) do
    try do
      document
      |> Floki.find("div[aria-label^=\"cena\"")
      |> Floki.text()
      |> String.trim_trailing("zł")
      |> String.trim_trailing()
      |> String.to_float()
    rescue
      _ -> nil
    end
  end
end
