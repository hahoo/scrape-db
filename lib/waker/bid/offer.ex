defmodule Waker.Bid.Offer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "offers" do
    field :link, EctoFields.URL
    field :price, :float
    field :title, :string
    field :accepted, :boolean, default: false
    field :type, Ecto.Enum, values: [:other, :allegro]

    timestamps()
  end

  @doc false
  def changeset(offer, attrs) do
    offer
    |> cast(attrs, [:title, :link, :price, :type, :accepted])
    |> validate_required([:title, :link, :price])
  end
end
