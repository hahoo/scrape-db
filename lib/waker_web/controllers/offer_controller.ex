defmodule WakerWeb.OfferController do
  use WakerWeb, :controller

  alias Waker.Bid
  alias Waker.Bid.{Offer, OfferLink}

  def index(conn, _params) do
    offers = Bid.list_offers()
    render(conn, "index.html", offers: offers)
  end

  def new(conn, %{"offer" => offer_params}) do
    changeset = Bid.change_offer(%Offer{}, offer_params)
    render(conn, "new.html", changeset: changeset)
  end

  def new(conn, _params) do
    changeset = Bid.change_offer(%Offer{})
    render(conn, "new.html", changeset: changeset)
  end

  def check(conn, %{"offer_link" => %{"link" => offer_link}}) do
    case Bid.check_offer(offer_link) do
      {:ok, changeset} ->
        render(conn, "new.html", changeset: changeset)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "check.html", changeset: changeset)
    end
  end

  def check(conn, _params) do
    changeset = Bid.change_link(%OfferLink{})
    render(conn, "check.html", changeset: changeset)
  end

  def create(conn, %{"offer" => offer_params}) do
    case Bid.create_offer(offer_params) do
      {:ok, offer} ->
        conn
        |> put_flash(:info, "Offer created successfully.")
        |> redirect(to: Routes.offer_path(conn, :show, offer))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    offer = Bid.get_offer!(id)
    render(conn, "show.html", offer: offer)
  end

  def edit(conn, %{"id" => id}) do
    offer = Bid.get_offer!(id)
    changeset = Bid.change_offer(offer)
    render(conn, "edit.html", offer: offer, changeset: changeset)
  end

  def update(conn, %{"id" => id, "offer" => offer_params}) do
    offer = Bid.get_offer!(id)

    case Bid.update_offer(offer, offer_params) do
      {:ok, offer} ->
        conn
        |> put_flash(:info, "Offer updated successfully.")
        |> redirect(to: Routes.offer_path(conn, :show, offer))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", offer: offer, changeset: changeset)

      :error ->
        conn
        |> put_flash(:error, "Something wrong happened")
        |> render("edit.html", offer: offer)
    end
  end

  def delete(conn, %{"id" => id}) do
    offer = Bid.get_offer!(id)
    {:ok, _offer} = Bid.delete_offer(offer)

    conn
    |> put_flash(:info, "Offer deleted successfully.")
    |> redirect(to: Routes.offer_path(conn, :index))
  end
end
