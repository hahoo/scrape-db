defmodule Waker.BidTest do
  use Waker.DataCase

  alias Waker.Bid

  describe "offers" do
    alias Waker.Bid.Offer

    import Waker.BidFixtures

    @invalid_attrs %{link: nil, price: nil, title: nil, type: nil}

    test "list_offers/0 returns all offers" do
      offer = offer_fixture()
      assert Bid.list_offers() == [offer]
    end

    test "get_offer!/1 returns the offer with given id" do
      offer = offer_fixture()
      assert Bid.get_offer!(offer.id) == offer
    end

    test "create_offer/1 with valid data creates a offer" do
      valid_attrs = %{
        link: "http://allegro.pl/oferta/item-101",
        price: 120.5,
        title: "some title",
        type: :allegro
      }

      assert {:ok, %Offer{} = offer} = Bid.create_offer(valid_attrs)
      assert offer.link == "http://allegro.pl/oferta/item-101"
      assert offer.price == 120.5
      assert offer.title == "some title"
      assert offer.type == :allegro
    end

    test "create_offer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Bid.create_offer(@invalid_attrs)
    end

    test "update_offer/2 with valid data updates the offer" do
      offer = offer_fixture()

      update_attrs = %{
        link: "http://allegro.pl/oferta/item-407",
        price: 456.7,
        title: "some updated title",
        type: :other
      }

      assert {:ok, %Offer{} = offer} = Bid.update_offer(offer, update_attrs)
      assert offer.link == "http://allegro.pl/oferta/item-407"
      assert offer.price == 456.7
      assert offer.title == "some updated title"
      assert offer.type == :other
    end

    test "update_offer/2 with invalid data returns error changeset" do
      offer = offer_fixture()
      assert {:error, %Ecto.Changeset{}} = Bid.update_offer(offer, @invalid_attrs)
      assert offer == Bid.get_offer!(offer.id)
    end

    test "delete_offer/1 deletes the offer" do
      offer = offer_fixture()
      assert {:ok, %Offer{}} = Bid.delete_offer(offer)
      assert_raise Ecto.NoResultsError, fn -> Bid.get_offer!(offer.id) end
    end

    test "change_offer/1 returns a offer changeset" do
      offer = offer_fixture()
      assert %Ecto.Changeset{} = Bid.change_offer(offer)
    end
  end
end
